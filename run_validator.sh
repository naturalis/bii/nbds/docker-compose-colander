#!/bin/bash
#
# run_validator.sh
#

working_dir="/opt/compose_projects/colander/compose"

echo "$(date -Iseconds)|INFO |run_validator.sh: started"
for pid in $(pidof -x run_validator.sh); do
  if [ $pid != $$ ]; then
    echo "$(date -Iseconds)|INFO |run_validator.sh: Process is already running with PID $pid"
    exit 0
  fi
done

cd "${working_dir}"
sudo /usr/bin/docker compose run validator_config
sudo /usr/bin/docker compose run json_schemas
sudo /usr/bin/docker compose run validator_import
sudo /usr/bin/docker compose run validator_process
echo "$(date -Iseconds)|INFO |run_validator.sh: finished"

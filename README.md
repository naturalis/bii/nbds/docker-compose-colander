# NBA colander

This is the docker-compose setup:

 - steward (set of bash scripts for coordinating job and data files)
 - validator (php environment for the validation)
 - infuser (java environment for loader)

All requirements needed are included in the containers. In the `.env` you can 
find where to find the job and data files.


## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* [https://github.com/gitleaks/gitleaks] already up to date!
* pre-commit installed at .git/hooks/pre-commit

